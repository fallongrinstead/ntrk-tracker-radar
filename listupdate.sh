#!/bin/bash
#Title : Ntrk-Tracker-Rader Git updater
#Description : Runs a git-pull for DDG Tracker Radar repo, then updates Ntrk-Tracker-Rader GitLab repo
#Author : QuidsUp
#Date Created : 11 September 2020
#Usage : bash listupdate.sh

#Directory structure:
#DDG: ~/duckduckgo/tracker-radar/
#Ntrk: ~/duckduckgo/ntrk-tracker-radar/
curdate=$(date +%Y-%m-%d)                                  #Date stamp for commit

#Run a git pull on DDG tracker radar folder
git -C ~/duckduckgo/tracker-radar/ pull

#Run ntrk_tracker_radar to generate updated block lists
python3 ntrk_tracker_radar.py

#Check file sizes to confirm data has been processed correctly
if [ "$(wc -l ddg_tracker_radar_confirmed.txt | cut -f1 -d\  )" -lt 1000 ]; then
  echo "File too small: ddg_tracker_radar_confirmed.txt, aborting"
  exit 1
fi
if [ "$(wc -l ddg_tracker_radar_high.hosts | cut -f1 -d\  )" -lt 300 ]; then
  echo "File too small: ddg_tracker_radar_high.hosts, aborting"
  exit 1
fi
if [ "$(wc -l ddg_tracker_radar_med.hosts | cut -f1 -d\  )" -lt 2000 ]; then
  echo "File too small: ddg_tracker_radar_med.hosts, aborting"
  exit 1
fi
if [ "$(wc -l ddg_tracker_radar_low.hosts | cut -f1 -d\  )" -lt 3000 ]; then
  echo "File too small: ddg_tracker_radar_low.hosts, aborting"
  exit 1
fi

git commit -a -m "updated for $curdate"
git push
